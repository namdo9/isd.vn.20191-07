package firstWeekCode.ticketCardInfo.vuducnguyen;
import firstWeekCode.ticketCardInfo.TicketCardInfo;


public class PrepaidCard extends TicketCardInfo {
	private float balance;

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
}
