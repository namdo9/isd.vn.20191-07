package dataLayer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.OnewayTicket;

public class OnewayTicketDAO {
	public static ArrayList<OnewayTicket> onewayTicketAll() throws ClassNotFoundException, SQLException{
		ArrayList<OnewayTicket> arr = new ArrayList<OnewayTicket>();
        String sql = "select * from one-wayticket";
     // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();
   
        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();
      
        ResultSet rs = statement.executeQuery(sql);
     // Duyệt trên kết quả trả về.
        while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
        	
        	OnewayTicket oneway = new OnewayTicket();
            oneway.setId(rs.getInt("Id"));
            oneway.setStatus(rs.getString("status"));
            oneway.setPrice(rs.getFloat("price"));
            oneway.setEmbarkation(rs.getString("embarkation"));
            oneway.setDisembarkation(rs.getString("disembarkation"));
            arr.add(oneway);
      
        }
        // Đóng kết nối
        connection.close();
        return arr;
	}
}
